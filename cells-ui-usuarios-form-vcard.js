import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import styles from './cells-ui-usuarios-form-vcard-styles.js';
import '@vcard-components/cells-util-behavior-vcard';
import '@vcard-components/cells-theme-vcard';
import '@bbva-web-components/bbva-form-field';
import '@bbva-web-components/bbva-form-password';
import '@vcard-components/cells-ui-autocomplete-vcard';
import '@cells-components/cells-select';
import '@bbva-web-components/bbva-button-default';
import '@vcard-components/cells-ui-message-alert-vcard';
/**
This component ...

Example:

```html
<cells-ui-usuarios-form-vcard></cells-ui-usuarios-form-vcard>
```

##styling-doc

* @customElement
* @polymer
* @LitElement
* @demo demo/index.html
*/
const utilBehavior = CellsBehaviors.cellsUtilBehaviorVcard;
export class CellsUiUsuariosFormVcard extends utilBehavior(LitElement) {
  static get is() {
    return 'cells-ui-usuarios-form-vcard';
  }

  // Declare properties
  static get properties() {
    return {
      oficinas: { type: Array },
      opcionesFind: { type: Array },
      opciones: { type: Array },
      newOpcionAdd: { type: Object },
      textButtonSave: { type: String },
      oficinaSelected: { type: Object },
      estadoSelected: { type: Object },
      perfilSelected: { type: Object },
      isUpdateReadOnly: { type: Boolean },
      usuario: { type: Object }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.oficinas = [];
    this.opcionesFind = [];
    this.opciones = [];
    this.usuario = {};
    this.textButtonSave = 'Registrar usuario';

    this.addEventListener('clear-value-autocomplete',(event)=>{
      if(event.detail.name === 'autocomplete-opcion'){
        this.newOpcionAdd = null;
      }else if(event.detail.name === 'autocomplete-oficina') {
        this.oficinaSelected = {};
      }
    });

    this.addEventListener(this.events.loadDataCellsSelectComplete,async (event)=> {
      console.log(`Evento escuchado[${this.events.loadDataCellsSelectComplete}]`, event.detail)
      if(this.usuario) {
        if(event.detail && event.detail.select && event.detail.select.id === 'estados-select'){
          this.estadoSelected = this.usuario.estado;
          this.getById('estados-select').options.forEach((opcion,index)=>{
            if(this.usuario && this.usuario.estado && this.usuario.estado._id && opcion.value === this.usuario.estado._id) {
              this.getById('estados-select').selected = index;
            }
          });
        } else if(event.detail && event.detail.select && event.detail.select.id === 'perfiles-select'){
          this.perfilSelected = this.usuario.perfil;
          this.getById('perfiles-select').options.forEach((_perfil,index)=>{
            if(this.usuario && this.usuario.perfil && this.usuario.perfil._id &&  _perfil.value === this.usuario.perfil._id) {
              this.getById('perfiles-select').selected = index;
            }
          });
        }
      }      
      
    })

    this.updateComplete.then(() => {
      this.inputSelectLoadData(this.events.loadDataCellsSelect, this);
    });
  }
  
  async setUserForm(userUpdate) {
    this.usuario = await userUpdate;
    this.textButtonSave = 'Aplicar cambios';
    this.getById('txtRegistro').value = this.usuario.registro;
    this.getById('txtNombreCompleto').value = this.usuario.nombreCompleto;
    this.getById('txtEmail').value = this.usuario.email;
    this.oficinaSelected = this.usuario.oficina;
    let oficinaAutocomplete = this.shadowRoot.querySelector('cells-ui-autocomplete-vcard[name="autocomplete-oficina"]');
    if(this.usuario && this.usuario.oficina && this.usuario.oficina.nombre) {
      oficinaAutocomplete.setValue(this.usuario.oficina.nombre);
    }
    this.opciones = this.usuario.opciones || [];
    if(this.isUpdateReadOnly){
      this.getById('txtRegistro').setAttribute('readonly', 'readonly');
      oficinaAutocomplete.addAtributo('readonly', 'readonly');
    }else {
      console.log(this.usuario);
      if(this.usuario.perfil.codigo !== this.ctts.perfiles.oficina) {
        oficinaAutocomplete.addAtributo('disabled','disabled');
      }
      if(this.usuario.estado){
        this.estadoSelected = this.usuario.estado;
        this.getById('estados-select').options.forEach((opcion,index)=>{
          if(opcion.value === this.usuario.estado._id) {
            this.getById('estados-select').selected = index;
          }
        });
      }
      
      if(this.usuario.perfil){
        this.perfilSelected = this.usuario.perfil;
        this.getById('perfiles-select').options.forEach((_perfil,index)=>{
          if(_perfil.value === this.usuario.perfil._id) {
            this.getById('perfiles-select').selected = index;
          }
        });
      }
    }
  }

  validateInput(idInput, msg, selector) {
    idInput = idInput || {};
    if(typeof idInput === 'object') {
        if(this.isEmpty(idInput)){
          this.getById('messageAlert').add({
            delay: 4000,
            message: msg
          }); 
          this.shadowRoot.querySelector(selector).focus();
          return null;
         }  
    } else {
      if(this.isEmpty(this.getInputValue(idInput))){
        this.getById('messageAlert').add({
          delay: 4000,
          message: msg
        }); 
        this.getById(idInput).focus();
        return null;
       }
       return this.getInputValue(idInput);
    }
    
  }

  validateAndBodyForm() {
    let registro = this.validateInput('txtRegistro', 'Debe informar el registro del usuario');
    if (!registro) {
      return;
    }
    let password = null;
    if(!this.usuario || !this.usuario._id) {
      password = this.validateInput('txtPassword', 'Debe informar el password de acceso.');
      if (!password) {
        return;
      }
    }
    let nombre = this.validateInput('txtNombreCompleto', 'Debe informar el nombre del usuario.');
    if (!nombre) {
      return;
    }
    let email = this.validateInput('txtEmail', 'Debe informar el email del usuario.');
    if (!email) {
      return;
    }

    this.validateInput(this.perfilSelected, 'Informe el perfil del usuario.', '#perfiles-select')

    if(this.isEmpty(this.perfilSelected)) {
      return;
    }
    if(this.perfilSelected.codigo === this.ctts.perfiles.oficina) {
      this.validateInput(this.oficinaSelected, 'Debe informar la oficina del usuario.', 'cells-ui-autocomplete-vcard[name="autocomplete-oficina"]')
      if(this.isEmpty(this.oficinaSelected)) {
        return;
      }
    }

    this.validateInput(this.estadoSelected, 'Informe el estado de activación del usuario.', '#estados-select')

    if(this.isEmpty(this.estadoSelected)) {
      return;
    }
    
   let bodyOpciones = [];
   if(!this.isUpdateReadOnly) {
    if(this.opciones.length > 0){
      bodyOpciones = this.opciones.map((item)=>{
        return item._id;
      });
     } 
   }
   
    let body = {
                userName: registro,
                nombreCompleto: nombre,
                registro: registro,
                email: email
            };

    if(this.perfilSelected.codigo === this.ctts.perfiles.oficina) {
      body.oficina = this.oficinaSelected._id;
    }        
    if(password) {
      body.password = password;
    }       
    if(this.usuario && this.usuario._id){
      body._id = this.usuario._id
    } 
    if(!this.isUpdateReadOnly) {
      body.opciones = bodyOpciones;
      body.perfil = this.perfilSelected._id;
      body.estado = this.estadoSelected._id
    }
    return body;
  }

  sendCreateAndUpdateUser() {
    let body = this.validateAndBodyForm();
    if(body) {
      this.dispatch(this.events.sendUserFormDataEvent, body);
    }
    
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('cells-ui-usuarios-form-vcard-shared-styles').cssText}
      ${getComponentSharedStyles('cells-theme-vcard').cssText}
    `;
  }

  onChangeOficina(event) {
    console.log('onChangeOficina', event.detail);
    this.oficinaSelected = event.detail;
  }

  onChangeEstado(event) {
    console.log('onChangeEstado', event.detail.value.record);
    this.estadoSelected = this.extract(event, 'detail.value.record');
  }

  onChangePerfil(event) {
    const perfil = this.extract(event, 'detail.value.record');
    console.log('perfil', perfil);
    this.perfilSelected = perfil;
    this.getById('txtPermisos').value = perfil.valor;
  }

  agregarItemOpcion() {
    if(this.newOpcionAdd) {
      let temp = this.opciones.filter((opcion)=>{
        return (opcion.nombre === this.newOpcionAdd.nombre);
      });
      if(temp.length === 0){
        this.opciones.push(this.newOpcionAdd);
      }
    }
    this.newOpcionAdd = null;
    let autocompleteOpc = this.shadowRoot.querySelector('cells-ui-autocomplete-vcard[name="autocomplete-opcion"]');
    if(autocompleteOpc) {
      autocompleteOpc.clearValue();
    }
  }

  removeOpcion(index) {
    if(this.opciones[index]){
      this.opciones.splice(index, 1);
      this.requestUpdate();
    }
  }

  onChangeNewOpcion(event) {
    this.newOpcionAdd = event.detail;
  }

  selectEstado() {
    if (this.isUpdateReadOnly) {
      return html`
      <cells-select 
        id = "estados-select"
        data-label="nombre"
        readonly
        data-value="_id" 
        data-is-key-value= "true"
        data-path = "${this.services.endPoints.valores.estadosActivacion}"
        label = "Estado"
        @selected-option-changed="${(e) => { this.selectedOptionChanged(e, (event) => { this.onChangeEstado(event); }); }}">
      </cells-select>`;
    }
    return html`
              <cells-select 
                id = "estados-select"
                data-label="nombre"
                data-value="_id" 
                data-is-key-value= "true"
                data-path = "${this.services.endPoints.valores.estadosActivacion}"
                label = "Estado"
                @selected-option-changed="${(e) => { this.selectedOptionChanged(e, (event) => { this.onChangeEstado(event); }); }}">
              </cells-select>`;
  }

  selectPerfil() {
    if (this.isUpdateReadOnly) {
      return html`
      <cells-select 
          id = "perfiles-select"
          data-label="nombre"
          readonly 
          data-value="_id" 
          data-is-key-value= "true"
          data-path = "${this.services.endPoints.valores.perfiles}"
          label = "Perfil"
          @selected-option-changed="${(e) => { this.selectedOptionChanged(e, (event) => { this.onChangePerfil(event);});}}">
      </cells-select>`;
    }
    return html`
                <cells-select 
                    id = "perfiles-select"
                    data-label="nombre" 
                    data-value="_id" 
                    data-is-key-value= "true"
                    data-path = "${this.services.endPoints.valores.perfiles}"
                    label = "Perfil"
                    @selected-option-changed="${(e) => { this.selectedOptionChanged(e, (event) => { this.onChangePerfil(event);});}}">
                </cells-select>`;
    
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <div class="section group">
        <div class="col span_6_of_12 panel-form panel-left">
          <div class = "panel-item-content">
            <bbva-form-field id = "txtRegistro" label = "Registro" ></bbva-form-field>
          </div>
          <div class = "panel-item-content">
            <bbva-form-password id = "txtPassword" label = "Contraseña" ></bbva-form-password>
          </div>
          <div class = "panel-item-content">
            <bbva-form-field id = "txtNombreCompleto" label = "Nombre Completo" ></bbva-form-field>
          </div>
          <div class = "panel-item-content">
            <bbva-form-field id = "txtEmail" label = "Correo electrónico" ></bbva-form-field>
          </div>
          <div class = "panel-item-content">
            <cells-ui-autocomplete-vcard 
                          name = "autocomplete-oficina"
                          @selected-item="${this.onChangeOficina}"
                          label="Oficina"
                          .items = ${this.oficinas}  
                          displayLabel="nombre"
                          displayValue="_id">
            </cells-ui-autocomplete-vcard>
          </div>        

          <div class = "panel-item-content">
            ${this.selectEstado()}
          </div>

          <div class = "panel-item-content">
            ${this.selectPerfil()}
          </div>

        </div>

        <div class="col span_6_of_12 panel-form panel-right">
          <div class = "sub-titulo">
            Permisos
          </div>  
          <div class = "panel-item-content">
            <bbva-form-field id = "txtPermisos" label = "Permisos asignados" value = "--" readonly ></bbva-form-field>
          </div>
          <div class = "sub-titulo">
            Opciones del sistema
          </div>  
          <div class = "panel-item-content">
            <div ?hidden = "${this.isUpdateReadOnly}" class = "panel-top panel-add-opciones" >
              <div class = "input-add-opc">
                <cells-ui-autocomplete-vcard 
                          name = "autocomplete-opcion"
                          bgClass = "white-bg" 
                          @selected-item="${this.onChangeNewOpcion}"
                          label="Opcion"
                          .items = ${this.opcionesFind}  
                          displayLabel="nombre"
                          displayValue="_id">
                </cells-ui-autocomplete-vcard>
              </div>
              <div class = "button-add-opc" >
                <button title = "Agregar opción" class = "button" @click = ${this.agregarItemOpcion} >
                    <cells-icon 
                      size = "20" 
                      icon="coronita:add"
                      ></cells-icon></button>
              </div>
            </div>
            <div class = "separator" ></div>
          <div class="table-container" style = "height:${this.isUpdateReadOnly ? '250px;':'210px;'}">
          <table class = "green">
              <thead>
                  <tr>
                      <th>Nombre</th>
                  </tr>
              </thead>
              <tbody>
                  ${this.opciones.map((opcion, index) => html`
                  <tr>
                      <td>
                        <div class = "text-add-opc">${opcion.nombre}</div>
                          <div ?hidden = ${this.isUpdateReadOnly} class = "button-trash-opc" >
                                <cells-icon 
                                title = "Eliminar"
                                @click = "${()=>this.removeOpcion(index)}"
                                class = "icon-trash"
                                  size = "16" 
                                  icon="coronita:trash"
                                  ></cells-icon>
                          </div>
                        </div>
                      </td>
                  </tr>
                  `)}
                  
                  ${this.opciones.length === 0 ? html` <tr><td><div style = "width: 100%;
                  text-align: center;
                  color: #666;">No hay opciones agregadas.</div></td></tr>` : html``}

              </tbody>
          </table>
          
      </div>
      
          </div>
        </div>

      </div>
      <div class = "container-buttons" >
          <bbva-button-default text = "${this.textButtonSave}" @click = "${this.sendCreateAndUpdateUser}" ></bbva-button-default>
      </div>
      <cells-ui-message-alert-vcard id = "messageAlert"></cells-ui-message-alert-vcard>
    `;
  }
}

// Register the element with the browser
customElements.define(CellsUiUsuariosFormVcard.is, CellsUiUsuariosFormVcard);
